import matplotlib.pyplot as mp
import numpy as np


#Contantes
c   = 721                                 #[J/kg*k]
rho = 1.29                                #[kg/m**3]
R = 1322.5                                #[ohm]

#Parametros
L = 0.05                                  #[m]
k = 0.13                                  #[W/(m*K)]
largo = 0.2                               #[m]
alto = 0.2                                #[m]
ancho = 0.2                               #[m]
vol = largo*alto*ancho                    #[m**3]
A = 2*L*largo+2*L*alto+2*L*ancho+8*L**2   #Area de la seccion transversal [m**2]

#Inicializacion del timepo y variable de estado
auxt = 86400              #[s]

t = np.arange(0,auxt)     #[s]
T = np.zeros(len(t)+1)    #[K]
T[0] = 273.15             #[K]
Tout = []                 #[K]
Taux = []
dt = 1

Tambiente = 26            #[°C]

for DT in range (0,auxt,dt):
    Ta = Tambiente + 273.15         #Temperatura ambiente [K]
    Taux.append(Ta-273.15)
    v = 5                          #Ecuacion de voltaje de fuente [V]
    i = v/R                         #Ecuacion  de corriente en el embobinado [A]
    qt =  i**2 * R                  #Flujo de calor segun la ley de joule [W]
    qr = (T[DT] - Ta)*k*A/L         #Flujo de calor que sale por las paredes [W]
    qc = qt - qr                    #Flujo de calor total [W]
    devT = qc/(rho*vol*c)           #Cambio de temperatura [K]

    Tout.append(T[DT]-273.15)       #Vector de salida de la temperatura [°C]
    T[DT+dt] = (T[DT] + devT*dt)    #Cambio en la temperatura para la siguiente iteracion [K]


mp.grid()
mp.plot(t,Tout)
mp.plot(t,Taux)
mp.legend(["T° incubadora","T° ambiente"])

mp.title("Variación de temperatura dentro de la incubadora")
mp.xlabel("Tiempo [s]")
mp.ylabel("Temperatura [°C]")
mp.show()